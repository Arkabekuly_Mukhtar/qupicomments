package mukhtar.qupicomments.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mukhtar on 3/23/2017.
 */

public class Comment{
    private Date date;
    private String dataInString;
    private String content;

    public Comment(Date date, String content) {
        this.date = date;
        SimpleDateFormat sdf = new SimpleDateFormat("dd.M.yyyy");
        this.dataInString = sdf.format(date);
        this.content = content;
    }

    public Comment(String dateInString, String content){
        SimpleDateFormat sdf = new SimpleDateFormat("dd.M.yyyy");
        try {
            this.date = sdf.parse(dateInString);
            this.dataInString = dateInString;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public String getDataInString() {
        return dataInString;
    }

    public String getContent() {
        return content;
    }
}