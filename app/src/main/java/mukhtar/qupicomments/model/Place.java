package mukhtar.qupicomments.model;

import android.graphics.Bitmap;
import java.util.ArrayList;

/**
 * Created by Mukhtar on 3/22/2017.
 */

public class Place {

    private String placeName;
    private double rating;
    private String address;
    private Bitmap image;

    private ArrayList<Comment> comments;

    public Place(String title, double rating, String address, Bitmap image) {
        this.placeName = title;
        this.rating = rating;
        this.address = address;
        this.image = image;
        comments = new ArrayList<>();
    }

    public Place(String title, double rating, String address) {
        this.placeName = title;
        this.rating = rating;
        this.address = address;
        comments = new ArrayList<>();
    }

    public void addComment(String date,String content){
        comments.add(new Comment(date,content));
    }

    public int getCommentsSize(){
        return comments.size();
    }

    public String getPlaceName() {
        return placeName;
    }

    public double getRating() {
        return rating;
    }

    public String getAddress() {
        return address;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public ArrayList<Comment> getComments(){
        return comments;
    }
}
