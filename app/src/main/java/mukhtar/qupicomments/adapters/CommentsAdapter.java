package mukhtar.qupicomments.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mukhtar.qupicomments.R;
import mukhtar.qupicomments.model.Comment;

/**
 * Created by Mukhtar on 3/23/2017.
 */

public class CommentsAdapter extends RecyclerView.Adapter <CommentsAdapter.CommentsViewHolder> {

    private ArrayList<Comment> commments;
    RelativeLayout leftColorArea;
    TextView dateOfComment;
    TextView contentOfComment;

    public CommentsAdapter(ArrayList <Comment> comments) {
        this.commments = comments;
    }

    @Override
    public CommentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View card = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_comment,parent,false);
        return new CommentsViewHolder(card);
    }

    @Override
    public void onBindViewHolder(CommentsViewHolder holder, int position) {
        Comment comment = commments.get(position);
        dateOfComment.setText(comment.getDataInString());
        contentOfComment.setText(comment.getContent());
        String [] colors = {"#00CFB5","#C26BD9","#CDCDCD"};
        leftColorArea.setBackgroundColor(Color.parseColor(colors[position%colors.length]));
    }

    @Override
    public int getItemCount() {
        return commments.size();
    }

    class CommentsViewHolder extends RecyclerView.ViewHolder{

        public CommentsViewHolder(View itemView) {
            super(itemView);

            leftColorArea = (RelativeLayout) itemView.findViewById(R.id.card_color_image_view);
            dateOfComment = (TextView) itemView.findViewById(R.id.card_date);
            contentOfComment = (TextView) itemView.findViewById(R.id.cars_content);
        }
    }
}
