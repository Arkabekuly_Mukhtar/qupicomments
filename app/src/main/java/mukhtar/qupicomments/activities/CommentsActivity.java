package mukhtar.qupicomments.activities;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import mukhtar.qupicomments.R;
import mukhtar.qupicomments.adapters.CommentsAdapter;
import mukhtar.qupicomments.model.Place;

public class CommentsActivity extends AppCompatActivity {
    public final static String LOG_TAG = "myLogs";
    Place barChikotka;

    RecyclerView recyclerViewComments;
    boolean isAdmin;
    RelativeLayout relativeLayoutChangable;

    AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView mainBackdrop;
    Typeface typeface ;
    TextView address;
    TextView placeName;
    TextView rating;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_activity);

        typeface = Typeface.createFromAsset(getAssets(),"ubuntu_c.ttf");
        appBarLayout = (AppBarLayout) findViewById(R.id.main_appbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.main_collapsing);

        placeName = (TextView)findViewById(R.id.text_view_place_name);
        placeName.setTypeface(typeface);
        address = (TextView)findViewById(R.id.text_view_address_place);
        address.setTypeface(typeface);
        rating = (TextView) findViewById(R.id.text_view_rating);
        rating.setTypeface(typeface);

        relativeLayoutChangable = (RelativeLayout)findViewById(R.id.relative_layout_changable);

        recyclerViewComments = (RecyclerView) findViewById(R.id.recycler_view_comments);
        recyclerViewComments.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,manager.getOrientation());
        recyclerViewComments.addItemDecoration(dividerItemDecoration);
        recyclerViewComments.setLayoutManager(manager);

         mainBackdrop = (ImageView) findViewById(R.id.main_backdrop);

        String placeName = "Бар чукотка Da Gudda Club";
        String address = "ул Ш. Калдаякова 1, Блок С";
        double rating = 7.6;
        Bitmap image = null;
        // load image
        try {
            // get input stream
            InputStream ims = getAssets().open("bar_chukotka.jpg");
            // load image as Drawable
            BitmapDrawable d = (BitmapDrawable) Drawable.createFromStream(ims, null);
            image = d.getBitmap();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
        String [] commentDates = {"12.10.2011","29.03.2013","23.04.2013","29.04.2013"};
        String [] commentContents = {"Collapsing title\n A title which is larger than the size of the screen. You can set the title to display via setTitle (CharSequence). The title appearance can be tweaked through the collapsedTextAppearance and expandedTextAppearance attributes.\n Content scrim\n A full-bleed scrim which is show or hidden when the scroll position has hit a certain threshold. You can change this via setContentScrim (Drawable).\n Status bar scrim\n A scrim which is the state of the bar. You can change this via setStatusBarScrim (Drawable). This only works on LOLLIPOP devices.",
        "Child views can opt to be pinned in space globally. This is useful when implementing a collapsing as it allows the Toolbar to be fixed in place even though this layout is moving. See COLLAPSE_MODE_PIN.\n Do not manually add views to the Toolbar at run time. We will add a 'dummy view' to the Toolbar which allows us to work out the available space for the title. This can interfere with any views which you add.",
        "Проще говоря, AppBarLayout это LinearLayout на стероидах, их элементы размещены вертикально, с определенными параметрами элементы могут управлять их поведением, когда содержимое прокручивается.\n Это может прозвучать запутано сначала, но как, — «Лучше один раз увидеть, чем сто раз услышать», к вашему вниманию .gif-пример:0",
        "Мы можем управлять поведением элементов AppbarLayout с помощью параметров: layout_scrollFlags. Значение: scroll в данном случае присутствует почти во всех элементах view, если бы этот параметр не был указан ни в одном из элементов AppbarLayout, он остался бы неизменным, позволяя прокручиваемому контенту проходить позади него.\n"};

        barChikotka = new Place(placeName,rating,address,image);

        for(int i = 0; i < commentDates.length;  i++){
            barChikotka.addComment(commentDates[i],commentContents[i]);
        }

        setPlaceDataToView(barChikotka);

    }

    public void setPlaceDataToView(Place place){
       // mainBackdrop.setImageBitmap(place.getImage());



       // Bitmap imageBitmapS = Bitmap.createScaledBitmap(place.getImage(),pixels,pixels,true);
       // collapsingToolbarLayout.setContentScrim(new BitmapDrawable(imageBitmapS));
        //collapsingToolbarLayout.setScrimVisibleHeightTrigger(pixels);

        mainBackdrop.setImageBitmap(place.getImage());
        address.setText(place.getAddress());
        rating.setText(place.getRating()+"");



       // Log.d(LOG_TAG,"scale density: "+scale+" image height in pixels"+mainBackdrop.getWidth());
       /* appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                float [] koefs = getKoefs();
                float KOEF_SCALE = koefs[0];//0.0018f;
                float KOEF_TRANS = koefs[1];//0.4f;

                relativeLayoutChangable.setTranslationY(verticalOffset*KOEF_TRANS);
                ImageView imageView = (ImageView) appBarLayout.findViewById(R.id.main_backdrop);
                imageView.setScaleY(1 + (verticalOffset * KOEF_SCALE));

            }
        });*/

        CommentsAdapter adapter = new CommentsAdapter(place.getComments());
        recyclerViewComments.setAdapter(adapter);
    }

    private double convertDP_PIXEL(int number, boolean isPixel){
        final float scale = getResources().getDisplayMetrics().density;
        if(isPixel){
            double dps = number / scale;
            return dps;
        }else{
            double pixels =  number * scale;
            return pixels;
        }
    }

    private float[] getKoefs(){
        float [] result = {0.0f,0.0f};
        float finalImageHeight = getResources().getDimension(R.dimen.toolbat_height);
        float initialImageHeight = getResources().getDimension(R.dimen.app_layout_height);
        Log.d(LOG_TAG,"final: "+finalImageHeight+"  initial: "+initialImageHeight);
        float maxOffSet = initialImageHeight-finalImageHeight-10;   //-10 страховка
        result[0] = (1-finalImageHeight/initialImageHeight)/(initialImageHeight-finalImageHeight);
        float differOfTrans = getResources().getDimension(R.dimen.final_margin_bottom_relative_layout_changable)
                - getResources().getDimension(R.dimen.initial_margin_bottom_relative_layout_changable);

        result[1] = differOfTrans/(maxOffSet+20); // +10 страховка
        Log.d(LOG_TAG,"scale: "+result[0]+"  trans: "+result[1]);
        return result;
    }



}
